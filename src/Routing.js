import React from "react";
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import Login from "./components/Login";

import HomePage from "./pages/HomePage";
import TaskFormPage from "./pages/TaskFormPage";
import TaskListPage from "./pages/TaskListPage";

export default function Routing() {
  return (
    <Router>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<PrivateRoute component={HomePage} />} />
        <Route
          path="/task/:taskId"
          element={<PrivateRoute component={TaskFormPage} />}
        />
        <Route
          path="/tasks/:filter"
          element={<PrivateRoute component={TaskListPage} />}
        />
        <Route path="/logout" element={<Navigate to="/login" />} />
        <Route path="*" element={<h1>GT - Error 404 - Pág. No Encontrada</h1>} />
      </Routes>
    </Router>
  );
}
