import { createContext } from "react";

export const AuthContext = createContext({
  auth: true,
});

export const AuthProvider = ({ children }) => {
  return <AuthContext.Provider>{children}</AuthContext.Provider>;
};
