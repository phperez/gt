import React from "react";
import Container from "../components/Container";
import TaskList from "../components/TaskList";

const TaskListPage = ( {status} ) => {
  return (
    <>
      <Container>
        <TaskList status={status} />
      </Container>
    </>
  );
};

export default TaskListPage;
