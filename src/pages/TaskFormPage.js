import React from "react";
import Container from "../components/Container";
import TaskForm from "../components/TaskForm";

const TaskFormPage = () => {
  return (
    <>
      <Container>
        <TaskForm />
      </Container>
    </>
  );
};

export default TaskFormPage;
