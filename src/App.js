import "./App.css";
import { AuthProvider } from "./context/AuthContext";
import Routing from "./Routing";

function App() {
  return (

      <div className="App">
        <Routing />
      </div>

  );
}

export default App;
