import * as React from 'react';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';

export default function Buscar( { searchValue, setSearchValue}) {
  const onSearchValueChange = (event) => {
    //console.log(event.target.value);
    setSearchValue(event.target.value);
  }

  return (
    <Paper
      component="form"
      sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400 }}
    >
      <InputBase
        onChange={onSearchValueChange}
        sx={{ ml: 1, flex: 1 }}
        placeholder="Buscar Título o Descripción"
        inputProps={{ 'aria-label': 'buscar' }}
      />
      <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
      <IconButton color="primary" sx={{ p: '10px' }} aria-label="textos">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
}
