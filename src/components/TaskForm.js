import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import { Button } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import { useNavigate, useParams } from "react-router-dom";
import { db } from "../firebase/FBConfig.js";
// import { collection, addDoc } from "firebase/firestore";
import { doc, getDoc, addDoc, setDoc, collection } from "firebase/firestore";

export default function TaskForm() {
  const taskId = useParams().taskId; // Toma el parametro :taskId de la url
  const navigate = useNavigate();
  const [f_alta, setFAlta] = useState(formatDateYMD());
  const [titulo, setTitulo] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [f_venc, setFVenc] = useState(formatDateYMD());
  const [estado, setEstado] = useState("P");

  const getDataById = async (id) => {
    const docRow = await getDoc(doc(db, "tareas", id));
    if (docRow.exists()) {
      setFAlta(docRow.data().f_alta);
      setTitulo(docRow.data().titulo);
      setDescripcion(docRow.data().descripcion);
      setFVenc(docRow.data().f_venc);
      setEstado(docRow.data().estado);
    } else {
      console.error("Error: Id inexistente");
    }
  };

  const row = {
    f_alta: f_alta,
    titulo: titulo,
    descripcion: descripcion,
    f_venc: f_venc,
    estado: estado,
  };

  const updateClick = async (e) => {
    e.preventDefault();
    try {
      taskId === "0"
        ? await addDoc(collection(db, "tareas"), row)
        : await setDoc(doc(db, "tareas", taskId), row);
      setTitulo("");
      setDescripcion("");
      alert("Tarea guardada correctamente!");
      navigate("/tasks/t");
    } catch (error) {
      console.log("Error al guardar tarea", error);
    }
  };
  const cancelClick = () => {
    navigate("/tasks/t");
  }

  useEffect(() => {
    if (taskId !== "0") {
      getDataById(taskId);
    }
  }, []);

  return (
    <FormControl variant="standard">
      <h2>{taskId === "0" ? "Nueva Tarea" : "Editar Tarea"}</h2>
      <TextField
        id="f_alta"
        name="f_alta"
        label="F.Alta"
        type="text"
        placeholder="aaaa-mm-dd"
        onChange={(e) => setFAlta(e.target.value)}
        value={f_alta}
        variant="standard"
      />
      <TextField
        id="titulo"
        name="titulo"
        label="Título"
        type="text"
        placeholder="ingrese un título"
        onChange={(e) => setTitulo(e.target.value)}
        value={titulo}
        variant="standard"
      />
      <TextField
        id="descripcion"
        name="descripcion"
        label="Descripción"
        type="text"
        placeholder="ingrese una descripción"
        onChange={(e) => setDescripcion(e.target.value)}
        value={descripcion}
        variant="standard"
      />
      <TextField
        id="f_venc"
        name="f_venc"
        label="F.Vencimiento"
        type="text"
        placeholder="aaaa-mm-dd"
        onChange={(e) => setFVenc(e.target.value)}
        value={f_venc}
        variant="standard"
      />{" "}
      <Stack spacing={1} direction="row">
        <Button
          onClick={cancelClick}
          variant="contained"
          size="small"
          color="secondary"
        >
          Cerrar
        </Button>
        <Button
          onClick={updateClick}
          variant="contained"
          size="small"
          color="success"
          type="submit"
        >
          Guardar
        </Button>
      </Stack>
    </FormControl>
  );
}

const formatDateYMD = (date = new Date()) => {
  let formatted_date =
    date.getFullYear() +
    "-" +
    (date.getMonth() + 1).toString().padStart(2, "0") +
    "-" +
    date.getDate().toString().padStart(2, "0");
  return formatted_date;
};
