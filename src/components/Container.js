import React from "react";
import NavBar from "./NavBar";

export default function Container( {children}) {
    return(
        <>
            <NavBar title='GT - Gestión de Tareas'
                           children={children} />
        </>
    )
}