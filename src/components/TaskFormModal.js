import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

export default function TaskForm({ open, setOpen, row }) {
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Editar Tarea</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="f-alta"
            label="F.Alta"
            helperText="formato aaaa-mm-dd"
            fullWidth
            variant="standard"
          />
          <TextField
            autoFocus
            margin="dense"
            id="titulo"
            label="Título"
            fullWidth
            variant="standard"
          />
          <TextField
            autoFocus
            margin="dense"
            id="descripcion"
            label="Descripción"
            fullWidth
            variant="standard"
          />
          <TextField
            autoFocus
            margin="dense"
            id="f-venc"
            label="F.Venc."
            helperText="formato aaaa-mm-dd"
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button onClick={handleClose} color="succes">Guardar</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
