import * as React from "react";
import Box from "@mui/material/Box";
import Input from "@mui/material/Input";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import {
  MailLock,
  Password as PasswordIcon,
  Login as LoginIcon,
} from "@mui/icons-material";
import { Button } from "@mui/material";
import { fbAuth } from "../firebase/FBConfig";
import { signInWithEmailAndPassword } from "firebase/auth";
import { AuthContext } from "../context/AuthContext.js";
import { useContext } from "react";

function Login() {

  //const [state, setState] = useContext(AuthContext);
  //console.log(state);
  const [user, setUser] = React.useState("");
  const [passw, setPassw] = React.useState("");
 
  const handleClick = (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(fbAuth, user, passw) //"phperez@itpatagonia.com" "Pablo1234"
      .then((userCredential) => {
        const user = userCredential.user;
        // console.log(user);

        window.location.href = "/";
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <Box sx={{ "& > :not(style)": { m: 1 } }}>
      <FormControl variant="standard">
        <InputLabel htmlFor="user">Usuario</InputLabel>
        <Input
          id="user"
          name="user"
          onChange={(e) => setUser(e.target.value)}
          value={user}
          startAdornment={
            <InputAdornment position="start">
              <MailLock />
            </InputAdornment>
          }
        />
        <TextField
          id="passw"
          name="passw"
          label="Contraseña"
          type="password"
          onChange={(e) => setPassw(e.target.value)}
          value={passw}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <PasswordIcon />
              </InputAdornment>
            ),
          }}
          variant="standard"
        />
        <Button
          onClick={handleClick}
          variant="contained"
          endIcon={<LoginIcon />}
        >
          Login
        </Button>
      </FormControl>
    </Box>
  );
}

export default Login;
