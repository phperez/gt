import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Buscar from "./Buscar";
import {
  Visibility as ViewIcon,
  Edit as EditIcon,
  Delete as DeleteIcon,
  CheckCircle as CheckIcon
} from "@mui/icons-material";
import { red } from '@mui/material/colors';
import { collection, doc, deleteDoc, onSnapshot, updateDoc } from "firebase/firestore";
import { db } from "../firebase/FBConfig";
import { useParams, Link } from "react-router-dom";

export default function TaskList() {
  const [xRows, setRows] = React.useState([]);

  const paramFilter = useParams().filter.toUpperCase(); // Toma el parametro :filter de la url

  const getData = () => {
    try {
      onSnapshot(collection(db, "tareas"), (snap) => {
        setRows(
          snap.docs.map((doc) => ({
            id: doc.id,
            f_alta: doc.data().f_alta,
            titulo: doc.data().titulo,
            descripcion: doc.data().descripcion,
            f_venc: doc.data().f_venc,
            estado: doc.data().estado,
          }))
        );
      });
    } catch (error) {
      console.log(error);
    }
  };

  React.useEffect(() => {
    getData();
  }, []);

  let rows =
    paramFilter === "T"
      ? xRows
      : xRows.filter((row) => row.estado === paramFilter);

  const [searchValue, setSearchValue] = React.useState("");

  let searchRows = [];

  if (!searchValue.length >= 1) {
    searchRows = rows;
  } else {
    searchRows = rows.filter((row) => {
      const tituloText = row.titulo.toLowerCase();
      const searchText = searchValue.toLowerCase();
      return tituloText.includes(searchText);
    });
  }

  const viewDescClick = (row) => {
    alert(row.descripcion);
  };

  const delTaskClick = async (row) => {
    await deleteDoc(doc(db, "tareas", row.id));
  };

  const changeStateClick = async (id) => await updateDoc(doc(db, "tareas", id), {estado: "R"});

  let setTitle = "Tareas";

  return (
    <>
      <Buscar searchValue={searchValue} setSearchValue={setSearchValue} />
      <h2>{setTitle}</h2>
      <TableContainer style={{ display: "flex" }}>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center">F. Alta</TableCell>
              <TableCell align="left">T&iacute;tulo</TableCell>
              <TableCell align="center">F. Venc.</TableCell>
              {paramFilter === "T" ? (
                <TableCell align="center">Estado</TableCell>
              ) : (
                <></>
              )}
              <TableCell align="center">Acciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {searchRows.map((row) => (
              <TableRow
                key={row.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell align="center">{row.f_alta}</TableCell>
                <TableCell align="left">{row.titulo}</TableCell>
                <TableCell align="center">{row.f_venc}</TableCell>
                {paramFilter === "T" ? (
                  <TableCell align="center">{row.estado}</TableCell>
                ) : (
                  <></>
                )}
                <TableCell align="center">
                  <ViewIcon color="primary" onClick={() => viewDescClick(row)} />
                  <Link to={`/task/${row.id}`}>
                    <EditIcon color="secondary" />
                  </Link>
                  <DeleteIcon sx={{ color: red[600] }} onClick={() => delTaskClick(row)} />
                  <CheckIcon color="success" onClick={() => changeStateClick(row.id)} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
