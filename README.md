## Instalacion de Material UI
npm install @mui/material @emotion/react @emotion/styled
npm install @mui/icons-material
<!-- Agregar en index.html -->
<link
rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
/>

## Libereria React Router
npm install react-router-dom

## Firebase
npm install firebase

## Ejecutar el proyecto
npm start

## Compilar para prod
npm run build